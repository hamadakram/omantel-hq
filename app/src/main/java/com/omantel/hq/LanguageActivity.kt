package com.omantel.hq

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.irozon.alertview.AlertActionStyle
import com.irozon.alertview.AlertStyle
import com.irozon.alertview.AlertView
import com.irozon.alertview.objects.AlertAction
import kotlinx.android.synthetic.main.activity_language.*
import java.util.*

class LanguageActivity : BaseActivity() {

    private var isEnglish: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_language)

        val isFromAppLaunch = intent?.extras?.getBoolean("isFromAppLaunch") ?: false
        tvArabic.setOnClickListener(this::onLanguageSelect)
        tvEnglish.setOnClickListener(this::onLanguageSelect)

        val language = LanguageManger.getLanguage(this)
        language?.let {
            setAppLanguage()
            if (isFromAppLaunch) startHomeActivity()
            when (it) {
                LanguageManger.Language.AR.value -> {
                    onLanguageSelect(tvArabic)
                }
                LanguageManger.Language.EN.value -> {
                    onLanguageSelect(tvEnglish)
                }
            }
        }
        btDone.setOnClickListener {
            LanguageManger.setLanguage(this, if (isEnglish) LanguageManger.Language.EN else LanguageManger.Language.AR)
            setAppLanguage()
            startHomeActivity()
        }
    }

    private fun setAppLanguage() {
        val locale = Locale(LanguageManger.getLanguage(this))
        val newConfig = Configuration(resources.configuration)
        Locale.setDefault(locale)
        newConfig.locale = locale
        newConfig.setLayoutDirection(locale)
        resources.updateConfiguration(newConfig, null)
    }

    private fun startHomeActivity() {
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun onLanguageSelect(view: View) {
        if (view is TextView) {
            when (view.text) {
                getString(R.string.arabic) -> {
//                    isEnglish = false
//                    tvEnglish.background = getDrawable(R.drawable.checkbox_unselected)
//                    tvArabic.background = getDrawable(R.drawable.checkbox_selected)
                    showAlert()
                }
                getString(R.string.english) -> {
                    isEnglish = true
                    tvArabic.background = getDrawable(R.drawable.checkbox_unselected)
                    tvEnglish.background = getDrawable(R.drawable.checkbox_selected)
                }
            }
        }
    }

    private fun showAlert() {
        val alert = AlertView("Coming Soon", "Arabic language will be available soon.", AlertStyle.DIALOG)
        alert.addAction(AlertAction("Ok", AlertActionStyle.DEFAULT) {
        })

        alert.show(this)
    }
}
