package com.omantel.hq

import android.content.Context
import android.os.Bundle
import android.util.DisplayMetrics
import android.widget.TableLayout
import android.widget.TableRow
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_content.*

class ContentActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_content)

        var content = ""

        val contentType = intent?.extras?.getString("contentType")
        when (contentType) {
            "ceo_message" -> {
                ivIcon.setImageResource(R.drawable.ic_quotation_mark_stroke)
                tvMessageTitle.text = getString(R.string.text_ceo_message)
                content = getString(R.string.ceo_message)
            }
            "cpo_message" -> {
                ivIcon.setImageResource(R.drawable.ic_quotation_mark_stroke)
                tvMessageTitle.text = getString(R.string.text_cpo_message)
                content = getString(R.string.cpo_message)
            }
            "location" -> {
                ivIcon.setImageResource(R.drawable.ic_location_stroke)
                tvMessageTitle.text = getString(R.string.location)
                content = getString(R.string.location_content)
            }
            "topography" -> {
                ivIcon.setImageResource(R.drawable.ic_topography_stroke)
                tvMessageTitle.text = getString(R.string.topography)
                content = getString(R.string.topography_content)
            }
            "external" -> {
                ivIcon.setImageResource(R.drawable.ic_external_storke)
                tvMessageTitle.text = getString(R.string.external_view)
                content = getString(R.string.external_content)
            }
            "hot_desk" -> {
                ivIcon.setImageResource(R.drawable.ic_hot_desk_stroke)
                tvMessageTitle.text = getString(R.string.hot_desk)
                content = getString(R.string.hot_desk_content)
            }
            "digitalisation" -> {
                ivIcon.setImageResource(R.drawable.ic_digitalisation_stroke)
                tvMessageTitle.text = getString(R.string.digitalisation)
                content = getString(R.string.digitalisation_content)
            }
            "restroom" -> {
                ivIcon.setImageResource(R.drawable.ic_restroom_stroke)
                tvMessageTitle.text = getString(R.string.restroom)
                content = "TBC"
            }
            "canteen" -> {
                ivIcon.setImageResource(R.drawable.ic_canteen_stroke)
                tvMessageTitle.text = getString(R.string.canteen)
                content = "TBC"
            }
            "auditorium" -> {
                ivIcon.setImageResource(R.drawable.ic_auditorium_stroke)
                tvMessageTitle.text = getString(R.string.auditorium)
                content = "TBC"
            }
            "others" -> {
                ivIcon.setImageResource(R.drawable.ic_others_stroke)
                tvMessageTitle.text = getString(R.string.others)
                content = "TBC"
            }
            "data_privacy" -> {
                ivIcon.setImageResource(R.drawable.ic_data_privacy_stroke)
                tvMessageTitle.text = getString(R.string.data_privacy)
                content = getString(R.string.data_privacy_content)
            }
            "facilities" -> {
                ivIcon.setImageResource(R.drawable.ic_facilities_stroke)
                tvMessageTitle.text = getString(R.string.facilities)
                content = getString(R.string.facilities_content)
            }
            "dosAndDonts"->{
                ivIcon.setImageResource(0)
                tvMessageTitle.text = getString(R.string.do_s_and_don_ts)
                content = getString(R.string.do_s_and_don_ts_content)
            }
        }
        tvMessage.setHtml(content)

        ivBack.setOnClickListener {
            finish()
        }
    }

    private fun convertDpToPixel(dp: Int, context: Context): Int {
        return (dp * (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)).toInt()
    }
}
