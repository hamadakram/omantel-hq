package com.omantel.hq

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity


class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed({
            val intent = Intent(this, LanguageActivity::class.java)
            intent.putExtra("isFromAppLaunch", true)
            startActivity(intent)
            finish()
        }, 2000)
    }
}
