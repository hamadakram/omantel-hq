package com.omantel.hq

import android.content.Context.MODE_PRIVATE
import android.R.id.edit
import android.content.Context
import android.content.SharedPreferences
import android.content.Context.MODE_PRIVATE


object LanguageManger {
    const val PREF_NAME = "Omantel HQ"
    const val LANGUAGE = "language"

    enum class Language(val value: String) {
        EN("en"),
        AR("ar")
    }

    fun setLanguage(context: Context, language: Language) {
        val editor = context.getSharedPreferences(PREF_NAME, MODE_PRIVATE).edit()
        editor.putString(LANGUAGE, language.value)
        editor.apply()
    }

    fun getLanguage(context: Context): String? {
        val prefs = context.getSharedPreferences(PREF_NAME, MODE_PRIVATE)
        return prefs.getString(LANGUAGE, null)

    }
}