package com.omantel.hq

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_feature.*
import android.opengl.ETC1.getHeight
import android.view.ViewTreeObserver
import android.widget.LinearLayout
import android.widget.RelativeLayout


class FeatureActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feature)

        others.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                others.viewTreeObserver.removeOnGlobalLayoutListener(this)
                val widht = others.width

                val secondColParam = others.layoutParams as LinearLayout.LayoutParams
                secondColParam.height = widht
                digitalisation.layoutParams = secondColParam
                canteen.layoutParams = secondColParam
                others.layoutParams = secondColParam

                val firstColParam = auditorium.layoutParams as LinearLayout.LayoutParams
                firstColParam.height = widht
                auditorium.layoutParams = firstColParam
                restroom.layoutParams = firstColParam
                hotDesking.layoutParams = firstColParam

            }
        })
        ivBack.setOnClickListener {
            finish()
        }

        hotDesking.setOnClickListener{
            val intent = Intent(this, ContentActivity::class.java)
            intent.putExtra("contentType", "hot_desk")
            startActivity(intent)
        }
        digitalisation.setOnClickListener{
            val intent = Intent(this, ContentActivity::class.java)
            intent.putExtra("contentType", "digitalisation")
            startActivity(intent)
        }
        restroom.setOnClickListener{
            val intent = Intent(this, ContentActivity::class.java)
            intent.putExtra("contentType", "restroom")
            startActivity(intent)
        }
        canteen.setOnClickListener{
            val intent = Intent(this, ContentActivity::class.java)
            intent.putExtra("contentType", "canteen")
            startActivity(intent)
        }
        auditorium.setOnClickListener{
            val intent = Intent(this, ContentActivity::class.java)
            intent.putExtra("contentType", "auditorium")
            startActivity(intent)
        }
        others.setOnClickListener{
            val intent = Intent(this, ContentActivity::class.java)
            intent.putExtra("contentType", "others")
            startActivity(intent)
        }
        dataPrivacy.setOnClickListener{
            val intent = Intent(this, ContentActivity::class.java)
            intent.putExtra("contentType", "data_privacy")
            startActivity(intent)
        }
        facilities.setOnClickListener{
            val intent = Intent(this, ContentActivity::class.java)
            intent.putExtra("contentType", "facilities")
            startActivity(intent)
        }
    }
}
