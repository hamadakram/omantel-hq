package com.omantel.hq

import android.content.Intent
import android.content.res.Configuration
import android.net.Uri
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_home.*
import java.util.*


class HomeActivity : BaseActivity() {

    private lateinit var language: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        language = LanguageManger.getLanguage(this) ?: ""

        btCeo.findViewById<TextView>(R.id.tvTitle).text = getString(R.string.text_ceo_message)
        btCpo.findViewById<TextView>(R.id.tvTitle).text = getString(R.string.text_cpo_message)
        btCpo.background = getDrawable(R.drawable.cpo_button_background)

        // Set up videos
        videoOne.findViewById<TextView>(R.id.tvTime).text = "03:16"
        videoOne.findViewById<ImageView>(R.id.videoBackground).setImageResource(R.drawable.video_one)
        videoOne.findViewById<TextView>(R.id.tvTitle).text = getString(R.string.video_one_title)

        videoTwo.findViewById<TextView>(R.id.tvTime).text = "00:45"
        videoTwo.findViewById<ImageView>(R.id.videoBackground).setImageResource(R.drawable.video_two)
        videoTwo.findViewById<TextView>(R.id.tvTitle).text = getString(R.string.video_two_title)

        ivChangeLanguage.setOnClickListener {
            val intent = Intent(this, LanguageActivity::class.java)
            startActivity(intent)
        }

        videoOne.setOnClickListener {
            val url = "http://omantel.indigo-oman.com/video/OmantelHQ.mp4"
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            intent.setDataAndType(Uri.parse(url), "video/mp4")
            startActivity(intent)
        }


        videoTwo.setOnClickListener {
            val url = "http://omantel.indigo-oman.com/video/OmantelHQ-English.mp4"
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            intent.setDataAndType(Uri.parse(url), "video/mp4")
            startActivity(intent)
        }

        btCeo.setOnClickListener {
            val intent = Intent(this, ContentActivity::class.java)
            intent.putExtra("contentType", "ceo_message")
            startActivity(intent)
        }
        btCpo.setOnClickListener {
            val intent = Intent(this, ContentActivity::class.java)
            intent.putExtra("contentType", "cpo_message")
            startActivity(intent)
        }
        btFeatures.setOnClickListener {
            val intent = Intent(this, FeatureActivity::class.java)
            startActivity(intent)
        }
        btLocations.setOnClickListener {
            val intent = Intent(this, ContentActivity::class.java)
            intent.putExtra("contentType", "location")
            startActivity(intent)
        }
        btTopography.setOnClickListener {
            val intent = Intent(this, ContentActivity::class.java)
            intent.putExtra("contentType", "topography")
            startActivity(intent)
        }
        btExternal.setOnClickListener {
            val intent = Intent(this, ContentActivity::class.java)
            intent.putExtra("contentType", "external")
            startActivity(intent)
        }
        btFloorPlan.setOnClickListener {
            val intent = Intent(this, FloorPlanActivity::class.java)
            startActivity(intent)
        }
        btDosAndDonts.setOnClickListener {
            val intent = Intent(this, ContentActivity::class.java)
            intent.putExtra("contentType", "dosAndDonts")
            startActivity(intent)
        }
        // Set up features
        setUpFeatures()
    }

    override fun onResume() {
        super.onResume()
        if (language != LanguageManger.getLanguage(this) ?: "") {
            language = LanguageManger.getLanguage(this) ?: ""
            val intent = intent
            finish()
            startActivity(intent)
        }
    }

    private fun setUpFeatures() {
        // Label
        btLocations.findViewById<TextView>(R.id.tvTitle).setText(R.string.location)
        btTopography.findViewById<TextView>(R.id.tvTitle).setText(R.string.topography)
        btExternal.findViewById<TextView>(R.id.tvTitle).setText(R.string.external_view)

        // Background
        btLocations.backgroundTintList = ContextCompat.getColorStateList(this, R.color.colorLocation)
        btTopography.backgroundTintList = ContextCompat.getColorStateList(this, R.color.colorTopography)
        btExternal.backgroundTintList = ContextCompat.getColorStateList(this, R.color.colorExternal)

        // Icons
        btLocations.findViewById<ImageView>(R.id.ivIcon).setImageResource(R.drawable.ic_location)
        btTopography.findViewById<ImageView>(R.id.ivIcon).setImageResource(R.drawable.ic_topography)
        btExternal.findViewById<ImageView>(R.id.ivIcon).setImageResource(R.drawable.ic_external)
    }
}
